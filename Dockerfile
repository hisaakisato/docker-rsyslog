FROM centos

MAINTAINER Hisaai Sato <hisaaki.sato@gmail.com>

RUN yum -y install rsyslog
RUN sed -e 's/#$ModLoad imudp/$ModLoad imudp/' \
        -e 's/#$UDPServerRun 514/$UDPServerRun 514/' \
        -e 's/#$ModLoad imtcp/$ModLoad imtcp/' \
        -e 's/#$InputTCPServerRun 514/$InputTCPServerRun 514/' \
        -i /etc/rsyslog.conf
ADD ./rsyslog.d/ /etc/rsyslog.d/
ADD ./logrotate.d/ /etc/logrotate.d/

EXPOSE 514/tcp 514/udp

CMD ["/usr/sbin/rsyslogd", "-n"]